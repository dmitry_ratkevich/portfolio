from django.db import models
from django.contrib.auth import get_user_model

User = get_user_model()


# класс сообщество
class Group(models.Model):
    # заголовок
    title = models.CharField(max_length=150)
    # уникальный адрес группы
    slug = models.SlugField(unique=True)
    # описание
    description = models.TextField()

    # при печати объекта на экран выводится поле "title"
    def __str__(self):
        return self.title


# класс публикация
class Post(models.Model):
    text = models.TextField()
    pub_date = models.DateTimeField("date published", auto_now_add=True)
    author = models.ForeignKey(
        User,
        # при удалении публикации, удаляются его зависмые объекты
        on_delete=models.CASCADE,
        related_name='posts'
    )
    group = models.ForeignKey(
        Group,
        blank=True,
        null=True,
        # Устанавливает ForeignKey в NULL, только при null=True, привязка к сообществу необязательна
        on_delete=models.SET_NULL,
        related_name='posts'
    )

from django.core.paginator import Paginator
from django.shortcuts import render, get_object_or_404

from .models import Post, Group


def index(request):
    post_list = Post.objects.select_related('group').all()
    paginator = Paginator(post_list, 10)
    page_number = request.GET.get('page')
    page = paginator.get_page(page_number)

    return render(
        request,
        'index.html',
        {'page': page, 'paginator': paginator}
    )


def group_post(request, slug):
    # функция get_object_or_404 возвращает сообщение об ошибке, если объект не найден
    group = get_object_or_404(Group, slug=slug)
    group_post_list = group.posts.all()
    paginator = Paginator(group_post_list, 5)
    page_number = request.GET.get('page')
    page = paginator.get_page(page_number)

    return render(
        request,
        'group.html',
        {'page': page, 'paginator': paginator, 'group': group}
    )


def page_not_found(request):
    return render(
        request,
        "misc/404.html",
        {"path": request.path},
        status=404
    )

from django.shortcuts import render
from django.urls import reverse_lazy
from django.views.generic import CreateView
from . forms import CreationForm


class SignUp(CreateView):
    #  form_class — из какого класса взять форму
    form_class = CreationForm
    #  функция reverse_lazy позволяет получить URL по параметру "name" функции path()
    #  где login — это параметр "name" в path()
    success_url = reverse_lazy("login")
    #  имя шаблона, куда будет передана переменная form с объектом HTML-формы
    template_name = "signup.html"
